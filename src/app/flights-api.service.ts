import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { IFlights } from './flights';
import 'rxjs/add/operator/map';

@Injectable()
export class FlightsApiService {

  private username = "Tomasz94";
  private password = "openSky123";
  private _url: string = "https://" + this.username + ":" + this.password + "@opensky-network.org/api/states/all";
  
  constructor(private http: HttpClient) { }

  getFlightsDetails(): Observable<IFlights[]> {
    return this.http.get<IFlights[]>(this._url);
  }

  getFlightDetails(icao24: any): Observable<IFlights[]> {
    return this.http.get<IFlights[]>(this._url + "?icao24=" + icao24);
  }

}
