export interface ITimes {
  plane_id: String,
  connection_time: String,
  create_date: Date
}