export interface IAirplanes {
  icao24: String,
  origin_country: String,
  create_date: Date
}