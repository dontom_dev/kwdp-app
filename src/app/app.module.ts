import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { FlightDetailsComponent } from './flight-details/flight-details.component';

import { HttpClientModule } from '@angular/common/http';
import { FlightsApiService } from './flights-api.service';
import { CustomApiService } from './custom-api.service';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbdModalBasic } from './modal-basic';

@NgModule({
  declarations: [
    AppComponent,
    FlightDetailsComponent,
    NgbdModalBasic
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule.forRoot(),
    FormsModule
  ],
  providers: [FlightsApiService, CustomApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
