import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { IAirplanes } from './airplanes';
import { ITimes } from './times';
import 'rxjs/add/operator/map';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'})
};

@Injectable()
export class CustomApiService {

  private _url: string = "https://still-stream-74569.herokuapp.com";

  constructor(private http: HttpClient) { }

  getAllAirplanes(): Observable<IAirplanes[]> {
    return this.http.get<IAirplanes[]>(this._url + '/api/airplanes');
  }

  getAirplaneDetails(icao24: any): Observable<IAirplanes[]> {
    return this.http.get<IAirplanes[]>(this._url + '/api/airplanes/' + icao24);
  }

  createAirplane(airplane) {
    let body = JSON.stringify(airplane);
    return this.http.post(this._url + '/api/airplanes', body);
  }

  getAirplaneTimes(): Observable<ITimes[]> {
    return this.http.get<ITimes[]>(this._url + '/api/times');
  }

  getAirplanesNTimes(limit: any): Observable<ITimes[]> {
    return this.http.get<ITimes[]>(this._url + '/api/times/limit/' + limit);
  }

  getSpecificAirplaneAllTimes(airplane:any): Observable<ITimes[]> {
    return this.http.get<ITimes[]>(this._url + '/api/times/' + airplane.icao24);
  }

  getSpecificAirplaneNTimes(airplane:any, limit: any): Observable<ITimes[]> {
    return this.http.get<ITimes[]>(this._url + '/api/times/' + airplane.icao24 + '/limit/' + limit);
  }

}
