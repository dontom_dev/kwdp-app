import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { FlightsApiService } from '../flights-api.service';
import { CustomApiService } from '../custom-api.service';
import { Chart } from 'chart.js';
import { Airplane } from '../airplane';

@Component({
  selector: 'app-flight-details',
  templateUrl: './flight-details.component.html',
  styleUrls: ['./flight-details.component.scss']
})
export class FlightDetailsComponent implements OnInit {

  public htmlRef;
  public flights = [];
  public airplanes = [];
  public timeOption = 4;
  public isLoadingData = false;
  public airplaneTimes = [];
  public isPlaneSelected = false;
  public error;
  public lastApiCallTime;
  public temp;
  public chart = [];
  public dataset = 0;
  public chartNumberOfPoints = 50; // number of points in chart
  public maximalWaitingTime = 100;
  public errorAirplane = false;

  public selectedPlaneId = { // currently seen in chart
    icao24: ""
  };
  public selectedPlane = { // currently selected in select form
    icao24: ""
  };
  public stats = {
    data: [],
    sum: 0,
    max: 0,
    median: 0,
    average: 0,
    errors: 0,
    warnings: 0
  }

  @Input() buttonWarning : Boolean;
  @Input() buttonConnectionLost : Boolean;
  @Input() buttonErrorConnection : Boolean;

  constructor(private _customApiService: CustomApiService, private elementRef: ElementRef) { }

  ngOnInit() {
    this.createChart();
    this.getAirplanes();
    this.initializeChart();
    this.buttonWarning = false;
    this.buttonConnectionLost = false;

    setInterval(() => {
      this.lastApiCallTime = this.getLastApiCallTime();
      this.updateCallTimes();
    }, 1000);
  }

  getAirplanes() {
    this._customApiService.getAllAirplanes().subscribe(
        data => this.airplanes = data, // success path
        error => {
          this.error = error
          this.errorAirplane = true;
        }
    );  
  }

  getSelectedAirplaneNTimes(limit) {
    this._customApiService.getSpecificAirplaneNTimes(this.selectedPlaneId, limit).subscribe(
      data => this.airplaneTimes = data, // success path
      error => {
        this.error = error
        this.errorAirplane = true;
      },
      () => {
        this.airplaneTimes.forEach(airplaneTime => {
          // Warning
          this.errorAirplane = false;

          let tempButton : HTMLElement = document.getElementById('temp-button');
          var modalElem = document.querySelectorAll('.modal')

          // Warning
          if(airplaneTime.connection_time > this.maximalWaitingTime && modalElem.length == 0) {
            if(!this.buttonWarning) { // if false
              tempButton.click(); 
            }
            this.buttonWarning = true;
          }
          else {
            this.buttonWarning = false;
          }
          
          this.addDataToChart(this.chart, this.getCurrentTime(), airplaneTime.connection_time);
          this.removeDataFromChart(this.chart);
        });
      }
    );  
  }

  getSelectedAirplaneAllTimes = function() {
    var date = new Date();
    var tempdate;
    if(this.timeOption == 1) { // today
      date.setDate(date.getDate()-1);
    }
    if(this.timeOption == 2) { // week
      date.setDate(date.getDate()-7);
    }
    if(this.timeOption == 3) { // month
      date.setDate(date.getDate()-30);
    }
    this.isLoadingData = true;
    this._customApiService.getSpecificAirplaneAllTimes(this.selectedPlaneId).subscribe(
      data => this.airplaneTimes = data, // success path
      error => this.error = error, // error path,
      () => {
        this.airplaneTimes.forEach(airplaneTime => {
          // stats
          if(this.timeOption < 4) {
            tempdate = Date.parse(airplaneTime.create_date);
            if(tempdate > date && airplaneTime.connection_time > 0) {
              this.stats.data.push(airplaneTime.connection_time);
            }
          }
          else {
            if(airplaneTime.connection_time > 0) {
              this.stats.data.push(airplaneTime.connection_time);
            }
          }
        });
        this.isLoadingData = false;

        // calclulate sum and max
        this.calcFunc();
      }
    );  
  }

  // show 50 last time results for selected plane on the chart
  changePlane() {
    this.selectedPlaneId = this.selectedPlane;
    this.isPlaneSelected = true;
    this.getSelectedAirplaneNTimes(this.chartNumberOfPoints);
  }

  addAirplane(form) : void {
    var currentTime = new Date();
    var airplane = new Airplane(form.value.icao24, form.value.origin_country, currentTime);

    this._customApiService.createAirplane(airplane).subscribe(
      data => {
        // refresh the list
        this.getAirplanes();
        return true;
      },
      error => this.error = error // error path,
    );
  }


  changeOptions(form) : void {
    this.maximalWaitingTime = form.value.maximalWaitingTime;
  }

  updateCallTimes() {
    if(this.isPlaneSelected) {
      this.temp = this.getSelectedAirplaneNTimes(1); // get 1 airplane time
    }
  }

  sortNumber(a,b) {
    return a - b;
  }

  calcFunc = function() {
    let statData = this.stats.data;

    statData.forEach(value => {
      this.stats.sum += parseInt(value);
      // calculate max
      if(value > this.stats.max) this.stats.max = value;
    });

    // calculate median
    statData.sort((a, b) => a - b);
    this.stats.median = statData[Math.round(statData.length/2)];

    // calculate average
    this.stats.average = Math.floor(this.stats.sum / statData.length);
  }

  refreshStats() {
    this.stats = {
      data: [],
      sum: 0,
      max: 0,
      median: 0,
      average: 0,
      errors: 0,
      warnings: 0
    }
    // get all data for current plane and calculate stats
    this.getSelectedAirplaneAllTimes();
  }

  initializeChart() {
    for (var i = 0; i < this.chartNumberOfPoints; i++) { 
      this.addDataToChart(this.chart, this.getCurrentTime(), 0);
    }
  }

  getLastApiCallTime() {
    return new Date().getTime() / 1000;
  }

  getCurrentTime() {
    let timeFormat = new Date().toISOString().slice(0,10) + ' ' + new Date().toISOString().slice(11,20)
    return timeFormat;
  }

  createChart() {
    this.htmlRef = this.elementRef.nativeElement.querySelector(`#canvas`);
    this.chart = new Chart(this.htmlRef, {
      type: 'line',
      data: {
        labels: [],
        datasets: [
          {
            data: [],
            borderColor: "#3cba9f",
            fill: false
          }
        ]
      },
      options: {
        spanGaps: true,
        legend: {
          display: false
        },
        scales: {
          xAxes: [{
            display: true
          }],
          yAxes: [{
            display: true,
            ticks: {
                suggestedMin: 0,
                suggestedMax: 100
            }
          }],
        }
      }
    });
  }

  addDataToChart(chart, label, data) {
    chart.data.labels.push(label);
    chart.data.datasets.forEach((dataset) => {
      dataset.data.push(data);
    });
    this.dataset++;
    chart.update();
  }

  removeDataFromChart(chart) {
    chart.data.labels.shift();
    chart.data.datasets.forEach((dataset) => {
      dataset.data.shift();
    });
    chart.update();
  }

}
