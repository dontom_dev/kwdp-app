import { ArrayType } from "@angular/compiler/src/output/output_ast";

export interface IFlights {
  time: number,
  states: Array<ArrayType>
}