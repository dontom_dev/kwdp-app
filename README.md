# Technical Specification


## General info
**Date:** 27/05/2018

**Author:** Tomasz Marcińczyk

**Project:** Airplanes monitoring system

**API App:** [https://still-stream-74569.herokuapp.com/](https://still-stream-74569.herokuapp.com/)

**API Source Code:** [https://bitbucket.org/dontom_dev/kwdp-api/src/](https://bitbucket.org/dontom_dev/kwdp-api/src/)

**Client App:** [https://nameless-eyrie-97566.herokuapp.com/](https://nameless-eyrie-97566.herokuapp.com/)

**Client App Source Code:** [https://bitbucket.org/dontom_dev/kwdp-app/src/](https://bitbucket.org/dontom_dev/kwdp-app/src/)



## Project description
Project consists of two custom applications database and external API provider, **_Database_**, **_API_** and **_Client App_**. Both are located on Heroku server, works remotely but can can be set locally on any mashine.

- **_Database_** is supposed to store airplanes and theirs connection times.

- **_API_** is responsible for real-time connector between [Open Sky API](https://opensky-network.org/apidoc/rest.html) and **_Database_**. Every second it gets connection time for all airplanes and sends them to the **_Database_**.

- **_Client App_** is web application written in Angular. It displays statistical data and diagrams about the specific airplane connection times. Additionally allows user to add new airplane to the **_Database_** and display warnings and server errors in order to improve users work performance.

- **_Server_** is running on [Heroku Cloud](http://heroku.com).

- **_Open Sky API_** is used to get data about airplanes and their connection times. It is developed and provided by [Open Sky Network Organization](https://opensky-network.org/).



## Technologies used
1. **Database** - Mongo DB
2. **API** - Node Js & Express Js
3. **Client App** - Angular 5
4. **Server** - Heroku Cloud
5. **Open Sky API** - External REST API



## Installation
In order to work on the project locally, one must install following list of dependencies:

- [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) (v2.14.1)

- [Node & npm](https://nodejs.org/en/download/) (v8.11.1 & v5.6.0)

- [Angular Cli](https://cli.angular.io/) (v1.7.3)

- [Mongo DB](https://docs.mongodb.com/manual/installation/) (v3.6.3)

- [Postman](https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop) **optional*



## Local development
Step 1 and 2 is not required. If one want run only local **_Client App_**, then skip to step 3. Else start with step 1.

1. **Local database**:

    a) Start Mongo `mongo`

    b) Start Mongod `mongod` 
    
    c) Create / use database: `use flights_monitoring`
    
    d) Create collections: 
    
    - `db.createCollection('airplanes')` 
    
    - `db.createCollection('times')`
        - display collections by `show collections`

    e) Insert data into collections: 
    
    - `db.airplanes.insert({icao24:'a8aac8', origin_country: "United States"})`
       
        - display airplanes collection by `db.airplanes.find().pretty()`
    
    - `db.times.insert({plane_id:'a8aac8', connection_time: "1"})`
        
        - display times collection by `db.times.find().pretty()`
    
    e) Clear all documents inside the collections:
    
    - `db.airplanes.remove({})`
    
    - `db.times.remove({})`
    

2. **Local API**:

    a) Clone [API](https://dontom_dev@bitbucket.org/dontom_dev/kwdp-api/src) repository
    
    - download all remote branches `git branch -a`
    
    - for local database `git checkout develop`
    
    - for server database `git checkout master`
    
    - run `npm install`
    
    b) Run `node app.js` 
    
    - navigate to `http://localhost:3000/`


3. **Local Client App**:
    
    a) Clone [Client App](https://bitbucket.org/dontom_dev/kwdp-app/src) repository
    
    - download all remote branches `git branch -a`
    
    - for local API `git checkout develop`
    
    - for server Heroku API  `git checkout master`
    
    - run `npm install`
    
    b) Run `ng serve`
    - navigate to `http://localhost:4200/`


4. **Open Sky API**:
    
    a) Run GET method inside Postman [https://opensky-network.org/api/states/all](https://opensky-network.org/api/states/all)



## Testing API requests

1. **Open Sky API**
    
    a) Get all data - `https://opensky-network.org/api/states/all`
    
    b) Get all data with user authorization - `https://[username]:[password]@opensky-network.org/api/states/all` **optional for testing*


2. **Local API**

    a) Get all airplanes data - `http://localhost:3000/api/airplanes`

    b) Get specific airplane data - `http://localhost:3000/api/airplanes/[id]`

    c) Get all connection times - `http://localhost:3000/api/times`

    d) Get connection times by airplane id - `http://localhost:3000/api/times/[id]`

    e) Get connection times by airplane id limited to **n** records - `http://localhost:3000/api/times/[id]/limit/[number]`

3. **Server API**

    a) Get all airplanes data - `https://still-stream-74569.herokuapp.com/api/airplanes`

    b) Get specific airplane data - `https://still-stream-74569.herokuapp.com/api/airplanes/[id]`

    c) Get all connection times - `https://still-stream-74569.herokuapp.com/api/times`

    d) Get connection times by airplane id - `https://still-stream-74569.herokuapp.com/api/times/[id]`

    e) Get connection times by airplane id limited to **n** records  `https://still-stream-74569.herokuapp.com/api/times/[id]/limit/[number]`

## Heroku deployment
- Create Heroku account
- `heroku login`
- `heroku create`
- `git add .`
- `git commit -m [...]`
- `git push heroku master`
- `heroku logs --tail` server logs
- `heroku run bash`

**API**
    
- In order to connect with Open Sky Network API, one need to set up environmental variables:

        - heroku config:set OPENSKY_API_PASSWORD=[username]

        - heroku config:set OPENSKY_API_USERNAME=[password]

        - heroku config:set DB_PASSWORD=[username]

        - heroku config:set DB_USERNAME=[password]


## Remarks
It may happen that Heroku API will not respons for some time. The reason is free cluster. Try to stimulate the server by naviating to [https://still-stream-74569.herokuapp.com/](https://still-stream-74569.herokuapp.com/). 
If you see 
> Server working... 

then it means that API is working correctly.

## Contact
To get help or more information, please contact with me via email: contact@dontom.pl.


## Tutorial references

- [Full API tutorial](https://www.youtube.com/watch?v=eB9Fq9I5ocs)

- [App Deployment to Heroku](https://www.youtube.com/watch?v=cBfcbb07Tqk)

- [API to Heroku](https://devcenter.heroku.com/articles/getting-started-with-nodejs#view-logs)

- [Chart.js tutorial](https://coursetro.com/posts/code/126/Let's-build-an-Angular-5-Chart.js-App---Tutorial)

- [Angular Bootstrap](https://www.youtube.com/watch?v=xgc1vnEcPCw)

- [Angular Forms](https://www.youtube.com/watch?v=hAaoPOx_oIw)

- [Angular http service](https://www.metaltoad.com/blog/angular-5-making-api-calls-httpclient-service)

- [Angular to Heroku](https://medium.com/@hellotunmbi/how-to-deploy-angular-application-to-heroku-1d56e09c5147)

- [Heroku commands](https://gist.github.com/23maverick23/5acc25a004680b4f2038)
