export class Airplane {

  constructor(
    public icao24: String,
    public origin_country: String,
    public create_date: Date
  ) {  }

}